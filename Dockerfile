FROM node:15-alpine

ENV MONGO_HOST $MONGO_HOST
ENV MONGO_PORT $MONGO_PORT
ENV MONGO_USER $MONGO_USER
ENV MONGO_PASSWORD $MONGO_PASSWORD
ENV MONGO_DATABASE $MONGO_DATABASE
ENV HOST $HOST
ENV PORT $PORT

RUN mkdir -p /home/app

COPY . /home/app

EXPOSE 5000

CMD ["node", "/home/app/dist-server/bin/www.js", "$ENV"]