# User api - expressjs

This small service, for while, connect express with mongodb using MongoClient.

##### Requirements:
- mongodb
- nodejs
- docker
- docker-compose

### Environment values
1. Rename **.env.dist** to **.env**;
2. Replace configurations mongo configurations on .env file;
3. Run the command below to run mongodb and mongodb express:
    ```
    docker-compose up
    ```
4. Access the admin environment for mongodb on the address http://localhost:8080;
5. Create the database **user-account** (same value from .env.dist MONGO_DATABASE);
6. Create the collection **user** and create some dummy data with fields **_id** (index auto incremented), **fistName**, **lastName**, **email**;

##### Running the project:
```
# first time
npm install
npm run dev
```

##### Create and run docker image
```
# create docker image
docker build -t IMAGE_NAME:IMAGE_VERSION .

# run docker container
docker run --name=CONTAINER_NAME --env-file=.env(.docker|.local|.dev|.prod) IMAGE_NAME:IMAGE_VERSION
```

##### On the browser:
```
http://localhost:5000
```


