'use strict';

import UserRepository from '../../infrastructure/repository/UserRepository';

class UserService {
    constructor(UserRepository) {
        this.userRepository = UserRepository;
    }

    findAll() {
        return this.userRepository.findAll();
    }
}

export default UserService;