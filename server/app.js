'use strict';

import NodeInjectionMiddleware from 'node-dependency-injection-express-middleware'
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import dotenv from 'dotenv';
import cors from 'cors';
import YAML from 'yamljs';
import swaggerUi from 'swagger-ui-express';
import indexRouter from './infrastructure/routes';
import usersRouter from './infrastructure/routes/users';

const app = express();

dotenv.config();

const options = {
    serviceFilePath: path.join(__dirname, 'resources/services.yml'),
    containerReferenceAsService: true,
    compile: true
};

const swaggerDocument = YAML.load(path.join(__dirname, 'resources/swagger.yml'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '../public')));
app.use(
    new NodeInjectionMiddleware(options).middleware()
);
app.use(cors());
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/', indexRouter);

app.use('/api/users', usersRouter);

export default app;
