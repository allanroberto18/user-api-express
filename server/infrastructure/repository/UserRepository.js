'use strict';

import MongoDBClient from '../mongodb/MongoDBClient';

class UserRepository {
    constructor(MongoDBClient) {
        this.client = MongoDBClient;
    }

    findAll() {
        const result = this.client.connect('users')
            .then((res) => {
                const users = res.find().toArray();

                return users;
            }
        );

        return result;
    }
}

export default UserRepository;