'use strict';

import express from 'express';

const router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  const container = req.container;
  const userService = container.get('domain.services.UserService');
  userService.findAll()
      .then(data => res.send(data));
});

export default router;
