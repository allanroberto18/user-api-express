'use strict';

import { MongoClient } from 'mongodb';

class MongoDBClient {
    connect(collectionName) {
        const host = process.env.MONGO_HOST;
        const port = process.env.MONGO_PORT;
        const user = process.env.MONGO_USER;
        const password = process.env.MONGO_PASSWORD;
        const database = process.env.MONGO_DATABASE;

        const mongoClient = new MongoClient(`mongodb://${user}:${password}@${host}:${port}`);

        console.log(
            `HOST: ${host}`,
            `PORT: ${port}`,
            `USER: ${user}`,
            `PASSWORD: ${password}`,
            `DATABASE: ${database}`,
            `COLLECTION: ${collectionName}`,
        );

        try {
            return mongoClient.connect()
                .then(client => {
                    const db = client.db(database);
                    const collection = db.collection(collectionName);

                    return collection;
                });
        } catch (error) {
            console.error(error);
        }

    }
}

export default MongoDBClient;